var express = require('express')
  , path = require('path')
  , favicon = require('static-favicon')
  , logger = require('morgan')
  , cookieParser = require('cookie-parser')
  , session = require('express-session')
  , bodyParser = require('body-parser')
  , messages = require('./lib/messages')
  , user = require('./lib/middleware/user')
  , basicAuth = require('basic-auth-connect')
  , User = require('./lib/user')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// middleware
app.use(favicon())
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(cookieParser('a super secret string'))
app.use(session())
app.use(messages)
app.use(express.static(path.join(__dirname, 'public')))
app.use('/api', basicAuth(User.authenticate))
app.use(user)

// routes
app.use('/api', require('./routes/api'))
app.use('/users', require('./routes/users'))
app.use('/register', require('./routes/register'))
app.use('/login', require('./routes/login'))
app.use('/logout', require('./routes/logout'))
app.use('/entries', require('./routes/entries'))
app.use('/', require('./routes/index'))

/// error handlers
if (process.env.ERROR_ROUTE) {
  app.get('/dev/error/503', function(req, res, next) {
    var err = new Error('Matched fake error')
    err.type = 'faux error 503'
    next(err)
  })
  app.get('/dev/error/500', function(req, res, next) {
    var err = new Error('Unmatched (default) fake error')
    err.type = 'faux error unmatched'
    next(err)
  })
}
app.use(require('./routes/error').notFound)


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(require('./routes/error').notFound)
app.use(require('./routes/error').server)

module.exports = app
