/**
 * Created by instancetype on 6/17/14.
 */
function parseField(field) {
  return field.split(/\[|\]/).filter(function(s) { return s })
}

function getField(req, field) {
  var val = req.body

  field.forEach(function(prop) {
    val = val[prop]
  })
  return val
}

exports.required = function(field) {
  field = parseField(field)
  return function(req, res, next) {
    if (getField(req, field)) next()

    else {
      res.error(field.join(' ') + ' is required.')
      res.redirect('back')
    }
  }
}

exports.lengthAbove = function(field, length) {
  field = parseField(field)

  return function(req, res, next) {
    if (getField(req, field).length > length) next()

    else {
      res.error(field.join(' ') + ' must have more than ' + length + ' characters.')
      res.redirect('back')
    }
  }
}


