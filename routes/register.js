/**
 * Created by instancetype on 6/16/14.
 */
var express = require('express')
  , User = require('../lib/user')
  , router = express.Router()

router.get('/', function(req, res) {
  res.render('register', { title: 'Register' })
})

router.post('/', function(req, res, next) {
  var data = req.body.user

  User.getByName(data.name, function(err, user) {
    if (err) return next(err)

    if (user.id) {
      res.error('The name \'' + data.name + '\' is already taken.')
      res.redirect('back')
    } else {
      user = new User({ name: data.name
                      , pass: data.pass
                      })

      user.save(function(err) {
        if (err) return next(err)
        req.session.uid = user.id
        res.redirect('/')
      })
    }
  })
})

module.exports = router