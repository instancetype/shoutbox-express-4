/**
 * Created by instancetype on 6/17/14.
 */
var express = require('express')
  , router = express.Router()
  , Entry = require('../lib/entry')
  , page = require('../lib/middleware/page')

module.exports = router

router.get( '/:page?'
  , page(Entry.count, 5)
  , function(req, res, next) {
    var page = req.page

    Entry.getRange(page.from, page.to, function(err, entries) {
      if (err) return next(err)

      res.render('index', { title: 'Entries'
        , entries: entries
      })
    })
  })
