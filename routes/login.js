/**
 * Created by instancetype on 6/16/14.
 */
var express = require('express')
  , User = require('../lib/user')
  , router = express.Router()

router.get('/', function(req, res) {
  res.render('login', { title: 'Login' })
})

router.post('/', function(req, res, next) {
  var data = req.body.user

  User.authenticate(data.name, data.pass, function(err, user) {
    if (err) return next (err)

    if (user) {
      req.session.uid = user.id
      res.redirect('/')
    } else {
      res.error('This username and password combo is invalid.')
      res.redirect('back')
    }
  })
})

module.exports = router