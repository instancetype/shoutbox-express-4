/**
 * Created by instancetype on 6/17/14.
 */
var express = require('express')
  , router = express.Router()
  , User = require('../lib/user')
  , entries = require('./entries')
  , Entry = require('../lib/entry')
  , page = require('../lib/middleware/page')
  , validate = require('../lib/middleware/validate')



router.get('/user/:id', function(req, res, next) {
  User.get(req.params.id, function(err, user) {
    if (err) return next(err)
    if (!user.id) return res.send(404)

    res.json(user)
  })
})
// Tested with httpie as below:
// http -a username:password -f POST http://localhost:3000/api/entry entry[title]='A Tale of Two Cities' entry[body]='It was the best of times...'
router.post( '/entry'
           , function(req, res, next) {
               var data = req.body.entry

               var entry = new Entry({ "username": res.locals.user.name
                                     , "title"   : data.title
                                     , "body"    : data.body
                                     })

               entry.save(function(err) {
                 if (err) return next(err)

                 if (req.remoteUser) res.json({ message: 'Entry added.' })
                 else res.redirect('/')
               })
})

router.get( '/entries/:page?'
          , page(Entry.count)
          , function(req, res, next) {
              var page = req.page

              Entry.getRange(page.from, page.to, function(err, entries) {
                if (err) return next(err)

                res.format({ json: function() {
                               res.send(entries)
                           }
                           , xml: function() {
                               res.render('entries/xml', { entries: entries })
                           }})
              })
  })

module.exports = router