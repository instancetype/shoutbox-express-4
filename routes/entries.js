/**
 * Created by instancetype on 6/17/14.
 */
var express = require('express')
  , router = express.Router()
  , Entry = require('../lib/entry')
  , page = require('../lib/middleware/page')
  , validate = require('../lib/middleware/validate')

module.exports = router

router.get('/', function(req, res) {
  res.render('post', { title: 'New Entry'})
})

router.post( '/'
           , validate.required('entry[title]')
           , validate.lengthAbove('entry[body]', 4)
           , function(req, res, next) {
               var data = req.body.entry

               var entry = new Entry({ "username": res.locals.user.name
                                     , "title"   : data.title
                                     , "body"    : data.body
                                     })

               entry.save(function(err) {
                 if (err) return next(err)

                 if (req.remoteUser) res.json({ message: 'Entry added.' })
                 else res.redirect('/')
               })
})