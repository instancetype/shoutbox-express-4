/**
 * Created by instancetype on 6/16/14.
 */
var express = require('express')
  , router = express.Router()

router.get('/', function(req, res) {

  req.session.destroy(function(err) {
    if (err) throw err

    res.redirect('/login')
  })
})

module.exports = router